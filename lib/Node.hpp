//
// Created by marek on 4/4/18.
//

#ifndef TREAP_NODE_HPP
#define TREAP_NODE_HPP

//the impl namespace separates the implementation from the user's namespace
namespace impl {
    struct rng {
    public:
        operator unsigned() const;
    };

    template<typename Item>
    struct Node {
        Item i;
        unsigned prior;
        Node *l;
        Node *r;

        Node(const Item &t_i);

        ~Node();

        Node<Item> *
        rotL();

        Node<Item> *
        rotR();

        void
        delSubTree();
    };

}

#include "Node.tpp"


#endif //TREAP_NODE_HPP
