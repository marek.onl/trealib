//
// Created by marek on 4/8/18.
//

namespace impl {
    template<typename Item, typename Treap>
    Iterator<Item, Treap>::Iterator(const Node <Item> *t_n, Treap *t_t) :
            m_n(t_n),
            m_t(t_t) {
    }

    template<typename Item, typename Treap>
    const Item &
    Iterator<Item, Treap>::operator*() const {
        return m_n->i;
    }

    template<typename Item, typename Treap>
    Iterator <Item, Treap> &
    Iterator<Item, Treap>::operator++() {
        m_n = m_t->getNext(m_n);
        return *this;
    }

    template<typename Item, typename Treap>
    Iterator <Item, Treap>
    Iterator<Item, Treap>::operator++(int) {
        auto tmp = *this;
        operator++();
        return tmp;
    }

    template<typename Item, typename Treap>
    Iterator <Item, Treap> &
    Iterator<Item, Treap>::operator--() {
        m_n = m_t->getPrev(m_n);
        return *this;
    }

    template<typename Item, typename Treap>
    Iterator <Item, Treap>
    Iterator<Item, Treap>::operator--(int) {
        auto tmp = *this;
        operator--();
        return tmp;
    }

    template<typename Item, typename Treap>
    bool
    Iterator<Item, Treap>::operator==(const Iterator &rhs) {
        return !operator!=(rhs);
    }

    template<typename Item, typename Treap>
    bool
    Iterator<Item, Treap>::operator!=(const Iterator &rhs) {
        if (!m_n && !rhs.m_n){
            return false;
        } else if ((!m_n && rhs.m_n) || (m_n && !rhs.m_n)) {
            return true;
        }

        return m_t->m_cmp(m_n->i.first, rhs.m_n->i.first) ||
               m_t->m_cmp(rhs.m_n->i.first, m_n->i.first);
    }

    template<typename Item, typename Treap>
    const Item *
    Iterator<Item, Treap>::operator->() const {
        return &operator*();
    }
}