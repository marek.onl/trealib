//
// Created by marek on 4/7/18.
//

#ifndef TREAP_MAIN_HPP
#define TREAP_MAIN_HPP

#include <iostream>

template<typename T>
struct Comparator {
    bool operator()(const T &lhs, const T &rhs) const { return lhs > rhs; }
};

struct Foo {
    int f;

    Foo(int t_f) : f(t_f) {};

    friend std::ostream &operator<<(std::ostream &t_os, const Foo &t_f) {
        t_os << t_f.f;
        return t_os;
    }
};

struct Key {
    int k;

    Key(int t_k) : k(t_k) {};

    friend std::ostream &operator<<(std::ostream &t_os, const Key &t_k) {
        t_os << t_k.k;
        return t_os;
    }

    friend bool operator<(const Key &l, const Key &r) { return l.k < r.k; }

    friend bool operator>(const Key &l, const Key &r) { return l.k > r.k; }
};


typedef std::pair<Key, Foo> P;

#endif //TREAP_MAIN_HPP
